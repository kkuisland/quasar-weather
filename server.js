const Koa = require('koa')
// const Router = require('koa-router')
const serve = require('koa-static')

const app = new Koa();
// const router = new Router();

// router.get('/api/sub', (ctx, next) =>
// {
//   ctx.body = '서브 페이지 입니다.'
// })

// app.use(router.routes());
// app.use(router.allowedMethods())
app.use(serve('/src-ssr/directives/render.js'))
const server = app.listen(3000, () =>
{
  console.log('열려라 !');
})